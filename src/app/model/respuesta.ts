export interface Respuesta{
    exito: number;
    mensaje: string;
    data: object[];
}
