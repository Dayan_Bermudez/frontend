import { Area } from "../area";
import { Rol } from "../rol";
import { TipoIdentificacion } from "../tipoIdentificacion";

export class NuevoUsuario{
    public nombre: string;
    public segundoNombre: string;
    public apellido: string;
    public segundoApellido: string;
    public tipoIdentificacion: TipoIdentificacion;
    public identificacion: number;
    public area: Area;
    public rol: Rol;
    public password: string;

  constructor() {
    this.area = new Area();
    this.rol = new Rol();
    this.tipoIdentificacion = new TipoIdentificacion();
  }
}
