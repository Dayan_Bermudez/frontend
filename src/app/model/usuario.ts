export interface Usuario{
  id:number;
  nombre: string;
  segundoNombre: string;
  apellido: string;
  segundoApellido: string;
  tipoIdentificacion: string;
  identificacion: number;
  area: string;
  rol: string;
  correoElectronico: string;
  fechaIngreso: string;
}
