export interface RequerimientoDeUsuario{
    id: number;
    usuario: string;
    requerimientosAsignados: number;
    requerimientosRealizados: number;
    requerimientosPendientes: number;
}