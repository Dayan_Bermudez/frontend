import { Usuario } from "./usuario";

export interface Requerimiento{
    id:number;
    descripcion: string;
    empleado: Usuario;
    fechaDeRegistro: string;
    responsable: Usuario;
    estado: string;
}