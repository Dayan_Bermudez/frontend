import { Component, Input, OnInit, ViewChild } from '@angular/core';
import { FormBuilder, FormGroup, MaxLengthValidator, MinLengthValidator, MinValidator, Validators } from '@angular/forms';
import { MatSnackBar } from '@angular/material/snack-bar';
import { ActivatedRoute, Router } from '@angular/router';
import { Area } from 'src/app/model/area';
import { NuevoUsuario } from 'src/app/model/createModel/nuevousuario';
import { Rol } from 'src/app/model/rol';
import { TipoIdentificacion } from 'src/app/model/tipoIdentificacion';
import { AreaService } from 'src/app/services/area/area.service';
import { RolService } from 'src/app/services/rol/rol.service';
import { TipoIdentificacionService } from 'src/app/services/tipoIdentificacion/tipoidentificacion.service';
import { UsuarioService } from 'src/app/services/usuario/usuario.service';

@Component({
  selector: 'app-formempleado',
  templateUrl: './formempleado.component.html',
  styleUrls: ['./formempleado.component.scss']
})
export class FormEmpleadoComponent implements OnInit {

  @Input("usuario") public usuarioUp: NuevoUsuario | undefined;

  id_empleado: number = 0;
  empleado = new NuevoUsuario();

  form: FormGroup;

  update: boolean = false;
  tiposIdentificacion: TipoIdentificacion[] = [];
  areas: Area[] = [];
  roles: Rol[] = [];

  constructor(
    private areaService: AreaService,
    private rolService: RolService,
    private tipoIdentificacionService: TipoIdentificacionService,
    private formBuilder: FormBuilder,
    private _route: ActivatedRoute,
    private usuarioService: UsuarioService,
    private router: Router,
    private snackBar: MatSnackBar
  ) { 
    
   }

  ngOnInit() {
    this.buildForm();
    this.getTipoIdentificaciones();
    this.getRoles();
    this.getAreas();
    let id_empleado = this._route.snapshot.paramMap.get('id');
    let numString = id_empleado != null ? id_empleado : '0';
    if (numString != '0') {
      this.id_empleado= Number(numString);
      this.update = true;
      this.getEmpleadoById(this.id_empleado);
    }
  }

  enviar(){
    if(this.form.valid){
      if(this.update){
        this.updateEmpleado();
      }else{
        this.createEmpleado();
      }
    }else{
      this.snackBar.open("POR FAVOR INGRESE TODOS LOS CAMPOS REQUERIDOS FORMULARIO",'',{duration:2000});
    }
  }

  updateEmpleado(){
    this.usuarioService.updateUsuario(this.id_empleado,this.empleado).subscribe(response=>{
      if(response.exito==1){
        this.snackBar.open("EMPLEADO ACTUALIZADO CON EXITO",'',{duration:2000});
        this.router.navigate(['/empleado']);
      }else{
        console.log(response);
      }
    });
  }

  createEmpleado(){
    this.usuarioService.createUsuario(this.empleado).subscribe(response =>{
      if(response.exito==1){
        this.snackBar.open("EMPLEADO ACTUALIZADO CON EXITO",'',{duration:2000});
        this.router.navigate(['/empleado']);
      }else{
        this.snackBar.open("NO SE PUDO REGISTRAR EL EMPLEADO",'',{duration:2000});
      }
    });
  }

  getTipoIdentificaciones(){
    this.tipoIdentificacionService.getTipos().subscribe(response =>{
      this.tiposIdentificacion = response;
      this.empleado.tipoIdentificacion.id = response[0].id;
      this.empleado.tipoIdentificacion.abreviatura = response[0].abreviatura;
      this.empleado.tipoIdentificacion.descripcion = response[0].descripcion;
    });
  }

  getRoles(){
    this.rolService.getRoles().subscribe(response =>{
      this.roles = response;
      this.empleado.rol.id = response[0].id;
      this.empleado.rol.nombre = response[0].nombre;
    })
  }

  getAreas(){
    this.areaService.getAreas().subscribe(response =>{
      this.areas = response;
      this.empleado.area.id = response[0].id;
      this.empleado.area.nombre = response[0].nombre;
    })
  }

  getEmpleadoById(id_empleado: number){
    this.usuarioService.getUsuarioById(id_empleado).subscribe(response =>{
      this.empleado = response;
    });
  }

  buildForm(){
    this.form = this.formBuilder.group({
      nombre: ['',Validators.compose([Validators.required,Validators.pattern('([a-zA-Z])*')])],
      segundoNombre: ['',Validators.minLength(0)],
      apellido: ['',Validators.compose([Validators.required,Validators.pattern('([a-zA-Z]|\s)*')])],
      segundoApellido: ['',Validators.minLength(0)],
      tipoIdentificacion: ['',Validators.required],
      identificacion: ['',Validators.required],
      area: ['',Validators.required],
      rol: ['',Validators.required],
      password: ['',Validators.minLength(0)],
    });
  }

}
