import { Component, OnInit, ViewChild } from '@angular/core';
import { MatDialog } from '@angular/material/dialog';
import { MatPaginator } from '@angular/material/paginator';
import { MatSnackBar } from '@angular/material/snack-bar';
import { MatSort } from '@angular/material/sort';
import { MatTableDataSource } from '@angular/material/table';
import { Router } from '@angular/router';
import { DialogDeleteComponent } from '../common/delete/dialogdelete.component';
import { Usuario } from '../model/usuario';
import {UsuarioService} from "../services/usuario/usuario.service";

@Component({
  selector: 'app-empleado',
  templateUrl: './empleado.component.html',
  styleUrls: ['./empleado.component.scss']
})
export class EmpleadoComponent implements OnInit {

  empleados: MatTableDataSource<Usuario>;

  public columnas: string[] =  ['id','nombre','segundoNombre','apellido','segundoApellido','tipoIdentificacion',
  'identificacion','area','correoElectronico','fechaIngreso','actions','requerimientos'];
  
  @ViewChild(MatSort) sort: MatSort;
  @ViewChild(MatPaginator) paginator: MatPaginator;

  filtroEstado: number = 1;

  constructor(
    private usuarioService: UsuarioService,
    private dialog: MatDialog,
    private snackBar: MatSnackBar,
    private router: Router
  ) { }

  ngOnInit() {
    this.getEmpleados();
  }

  getEmpleados(){
    this.usuarioService.getUsuarios().subscribe(response => {
      if(response.length!=0){
        this.paginatorFuncion(response);
      }else{
        this.snackBar.open("NO SE HAN CARGADO LOS EMPLEADOS",'',{duration:2000});
      }
    });
  }

  openDelete(id_usuario:number){
    const dialogRef = this.dialog.open(DialogDeleteComponent,{});
    dialogRef.afterClosed().subscribe(result => {
      if(result){
        this.usuarioService.deleteUsuario(id_usuario).subscribe(response =>{
          if(response.exito == 1){
            this.snackBar.open("EMPLEADO ELIMINADO CON EXITO",'',{duration:2000});
            this.getEmpleados();
          }
        })
      }
    });
  }

  paginatorFuncion(response: Usuario[]){
    this.empleados = new MatTableDataSource(response);
    this.empleados.sort = this.sort;
    this.empleados.paginator = this.paginator;
  }

  cambiarEstado(estado: number){
    this.filtroEstado = estado;
  }

  obtenerEmpleados(empleados: MatTableDataSource<Usuario>){
    this.empleados = empleados;
  }

  verRequerimientos(empleado:Usuario){
    window.localStorage.setItem("empleado",JSON.stringify(empleado));
    this.router.navigate(['/detalle',empleado.id]); 
  }
  
}
