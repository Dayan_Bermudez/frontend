import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { LoginComponent } from './auth/login/login.component';
import { DetallesRequerimientoComponent } from './detallesrequerimiento/detallesrequerimiento.component';
import { EmpleadoComponent } from './empleado/empleado.component';
import { FormEmpleadoComponent } from './empleado/formempleado/formempleado/formempleado.component';
import { HomeComponent } from './home/home.component';
import { RequerimientoDeUsuarioComponent } from './requerimientodeusuario/requerimientodeusuario.component';
import { ProdGuardService as guard } from "./security/guards/prod-guard.service";

const routes: Routes = [
  { path: '', redirectTo: '/login', pathMatch: 'full' },
  { path: 'home', component: HomeComponent, canActivate: [guard], data: { expectedRol: ['admin', 'empleado'] } },
  { path: 'empleado', component: EmpleadoComponent, canActivate: [guard], data: { expectedRol: ['admin'] }  },
  { path: 'formempleado', component: FormEmpleadoComponent, canActivate: [guard], data: { expectedRol: ['admin'] }  },
  { path: 'formempleado/:id', component: FormEmpleadoComponent, canActivate: [guard], data: { expectedRol: ['admin','empleado'] }  },
  { path: 'requerimiento', component: RequerimientoDeUsuarioComponent, canActivate: [guard], data: { expectedRol: ['admin', 'empleado'] }  },
  { path: 'detalle/:id', component: DetallesRequerimientoComponent, canActivate: [guard], data: { expectedRol: ['admin', 'empleado'] }  },
  { path: 'login', component: LoginComponent}
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
