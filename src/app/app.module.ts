import { NgModule, ViewChild } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { EmpleadoComponent } from './empleado/empleado.component';
import { MenuComponent } from './menu/menu.component';
import { DialogDeleteComponent } from "../app/common/delete/dialogdelete.component";


import { HttpClientModule } from "@angular/common/http";


import { MatToolbarModule } from "@angular/material/toolbar";
import { MatTableModule } from "@angular/material/table";
import { MatButtonModule } from '@angular/material/button';
import { MatDialogModule } from "@angular/material/dialog";
import { MatInputModule } from "@angular/material/input";
import { MatSnackBarModule } from "@angular/material/snack-bar";
import { HomeComponent } from './home/home.component';
import { FormEmpleadoComponent } from './empleado/formempleado/formempleado/formempleado.component';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { MatPaginatorModule } from '@angular/material/paginator';
import { MatSortModule } from '@angular/material/sort';
import { RequerimientoDeUsuarioComponent } from './requerimientodeusuario/requerimientodeusuario.component';
import { DetallesRequerimientoComponent } from './detallesrequerimiento/detallesrequerimiento.component';
import {MatCardModule} from '@angular/material/card';
import {MatGridListModule} from '@angular/material/grid-list';
import { DialogRequerimientoComponent } from './requerimientodeusuario/dialog/dialogrequerimiento/dialogrequerimiento.component';
import {MatCheckboxModule} from '@angular/material/checkbox';
import {MatRadioModule} from '@angular/material/radio'; 
import { FiltroEmpleadosComponent } from './common/filtroempleados/filtroEmpleados/filtroEmpleados.component';
import { interceptorProvider } from './security/interceptors/prod-interceptor.service';
import { LoginComponent } from './auth/login/login.component';


@NgModule({
  declarations: [					
    AppComponent,
      EmpleadoComponent,
      MenuComponent,
      HomeComponent,
      DialogDeleteComponent,
      FormEmpleadoComponent,
      RequerimientoDeUsuarioComponent,
      DetallesRequerimientoComponent,
      DialogRequerimientoComponent,
      FiltroEmpleadosComponent,
      LoginComponent
   ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    BrowserAnimationsModule,
    MatToolbarModule,
    MatTableModule,
    MatButtonModule,
    MatDialogModule,
    MatInputModule,
    MatSnackBarModule,
    HttpClientModule,
    FormsModule,
    ReactiveFormsModule,
    MatPaginatorModule,
    MatSortModule,
    MatCardModule,
    MatGridListModule,
    MatButtonModule,
    MatCheckboxModule,
    MatRadioModule
  ],
  providers: [interceptorProvider],
  bootstrap: [AppComponent]
})
export class AppModule { }
