import { Component, OnInit } from '@angular/core';
import { MatSnackBar } from '@angular/material/snack-bar';
import { Router } from '@angular/router';
import { LoginUsuario } from 'src/app/security/model/loginusuario';
import { AuthService } from 'src/app/security/services/auth.service';
import { TokenService } from 'src/app/security/services/token.service';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss']
})
export class LoginComponent implements OnInit {

  isLogged = false;
  isLoginFail = false;
  loginUsuario: LoginUsuario | undefined;
  nombreUsuario: string = '';
  password: string = '';
  rol: string[] = [];

  constructor(
    private tokenService: TokenService,
    private authService: AuthService,
    private router: Router,
    private snackBar: MatSnackBar
  ) { }

  ngOnInit():void {
    if(this.tokenService.getToken()!='undefined'){
      this.isLogged = true;
      this.isLoginFail = false;
      this.rol = this.tokenService.getAuthorities();
      this.router.navigate(['/home']);
    }
  }

  onLogin():void {
    if(this.nombreUsuario.length!=0 && this.password.length!=0){
      this.loginUsuario =  new LoginUsuario(this.nombreUsuario,this.password);
      this.authService.login(this.loginUsuario).subscribe(response => {
        if(response != null){
          this.isLogged = true;
          this.isLoginFail = false;
          
          this.tokenService.setId(response.id);
          this.tokenService.setToken(response.token);
          this.tokenService.setUserName(response.nombreUsuario);
          this.tokenService.setAuthorities(response.authorities[0]);
          this.rol = response.authorities;
          this.router.navigate(['/home']);
        }else{
          this.snackBar.open('INGRESE NUEVAMENTE SU NOMBRE DE USUARIO Y CONTRASEÑA','',{duration: 2000});
        }
      });
    }else{
      this.snackBar.open('VERIFIQUE SU NOMBRE DE USUARIO Y CONTRASEÑA','',{duration: 2000})
    }
  }
}