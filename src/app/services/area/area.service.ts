import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { Area } from 'src/app/model/area';

@Injectable({
  providedIn: 'root'
})
export class AreaService {

  url: string = 'http://localhost:8080/area';

  constructor(
    private _http: HttpClient
  ) { }

  getAreas():Observable<Area[]>{
    return this._http.get<Area[]>(this.url);
  }

}
