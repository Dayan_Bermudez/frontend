import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { TipoIdentificacion } from 'src/app/model/tipoIdentificacion';

@Injectable({
  providedIn: 'root'
})
export class TipoIdentificacionService {

  url: string = 'http://localhost:8080/tipos/identificacion';

  constructor(
    private _http: HttpClient
  ) { }

  getTipos():Observable<TipoIdentificacion[]>{
    return this._http.get<TipoIdentificacion[]>(this.url);
  }

}
