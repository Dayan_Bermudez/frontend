import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { Rol } from 'src/app/model/rol';

@Injectable({
  providedIn: 'root'
})
export class RolService {

  url: string = 'http://localhost:8080/rol';

  constructor(
    private _http: HttpClient
  ) { }

  getRoles():Observable<Rol[]>{
    return this._http.get<Rol[]>(this.url);
  }
}
