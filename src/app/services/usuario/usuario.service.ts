import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { NuevoUsuario } from 'src/app/model/createModel/nuevousuario';
import { Respuesta } from 'src/app/model/respuesta';
import { Usuario } from 'src/app/model/usuario';

const httpOption = {
  headers: new HttpHeaders({
      'Content-Type':'application/json'
  })
}

@Injectable({
  providedIn: 'root'
})
export class UsuarioService {

  url: string = 'http://localhost:8080/usuario';

  constructor(
    private _http: HttpClient
  ) { }

  getUsuarios():Observable<Usuario[]>{
    return this._http.get<Usuario[]>(this.url);
  }

  getUsuarioById(id_empleado: number):Observable<NuevoUsuario>{
    return this._http.get<NuevoUsuario>(`${this.url}/${id_empleado}`);
  }

  getUsuarioByNombre(nombre: string):Observable<Usuario[]>{
    return this._http.get<Usuario[]>(`${this.url}/getByNombre/${nombre}`);
  }

  getUsuarioByApellido(apellido: string):Observable<Usuario[]>{
    return this._http.get<Usuario[]>(`${this.url}/getByApellido/${apellido}`);
  }

  getUsuarioBySegundoNombre(segundoNombre: string):Observable<Usuario[]>{
    return this._http.get<Usuario[]>(`${this.url}/getBySegundoNombre/${segundoNombre}`);
  }

  getUsuarioBySegundoApellido(segundoApellido: string):Observable<Usuario[]>{
    return this._http.get<Usuario[]>(`${this.url}/getBySegundoApellido/${segundoApellido}`);
  }

  getUsuarioByIdentificacion(identificacion: string):Observable<Usuario[]>{
    return this._http.get<Usuario[]>(`${this.url}/getByIdentificacion/${identificacion}`);
  }

  getUsuarioByCorreoElectronico(correoElectronico: string):Observable<Usuario[]>{
    return this._http.get<Usuario[]>(`${this.url}/getByCorreoElectronico/${correoElectronico}`);
  }

  getUsuarioByEstado(estado: number):Observable<Usuario[]>{
    return this._http.get<Usuario[]>(`${this.url}/getByEstado/${estado}`);
  }

  getUsuarioByTipoId(tipo: number):Observable<Usuario[]>{
    return this._http.get<Usuario[]>(`${this.url}/getByTipoId/${tipo}`);
  }

  createUsuario(newUsuario: NuevoUsuario):Observable<Respuesta>{
    return this._http.post<Respuesta>(this.url+'/nuevo',newUsuario);
  }

  updateUsuario(id: number,upUsuario: NuevoUsuario):Observable<Respuesta>{
    return this._http.put<Respuesta>(`${this.url}/${id}`,upUsuario,httpOption);
  }

  deleteUsuario(id: number):Observable<Respuesta>{
    return this._http.delete<Respuesta>(`${this.url}/${id}`);
  }

  

}
