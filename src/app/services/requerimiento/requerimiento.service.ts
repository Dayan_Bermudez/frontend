import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { NuevoRequerimiento } from 'src/app/model/createModel/nuevorequerimiento';
import { Requerimiento } from 'src/app/model/requerimiento';
import { Respuesta } from 'src/app/model/respuesta';

const httpOption = {
  headers: new HttpHeaders({
      'Content-Type':'application/json'
  })
}

@Injectable({
  providedIn: 'root'
})
export class RequerimientoService {

  url: string = 'http://localhost:8080/requerimiento';

  constructor(
    private _http: HttpClient
  ) { }

  getRequerimientos():Observable<Requerimiento[]>{
    return this._http.get<Requerimiento[]>(this.url);
  }

  getRequerimientosByEmpleado(id_empleado: number):Observable<Requerimiento[]>{
    return this._http.get<Requerimiento[]>(`${this.url}/empleado/${id_empleado}`);
  }

  getRequerimientosById(id_requerimiento: number):Observable<Requerimiento>{
    return this._http.get<Requerimiento>(`${this.url}/${id_requerimiento}`);
  }

  createRequerimiento(requerimiento: NuevoRequerimiento):Observable<Respuesta>{
    return this._http.post<Respuesta>(this.url,requerimiento,httpOption);
  }

  updateRequerimiento(id:number,requerimiento: NuevoRequerimiento):Observable<Respuesta>{
    return this._http.put<Respuesta>(`${this.url}/${id}`,requerimiento,httpOption);
  }

  deleteRequerimiento(id: number):Observable<Respuesta>{
    return this._http.delete<Respuesta>(`${this.url}/${id}`);
  }

}
