import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { RequerimientoDeUsuario } from 'src/app/model/requerimientodeusuario';

@Injectable({
  providedIn: 'root'
})
export class RequerimientoDeUsuarioService {

  url: string = 'http://localhost:8080/requerimiento/usuario';

  constructor(
    private _http: HttpClient
  ) { }

  getRequerimientosDeUsuario():Observable<RequerimientoDeUsuario[]>{
    return this._http.get<RequerimientoDeUsuario[]>(this.url);
  }
}
