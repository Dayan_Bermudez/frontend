import { Component, OnInit, ViewChild } from '@angular/core';
import { MatDialog } from '@angular/material/dialog';
import { MatPaginator } from '@angular/material/paginator';
import { MatSnackBar } from '@angular/material/snack-bar';
import { MatSort } from '@angular/material/sort';
import { MatTableDataSource } from '@angular/material/table';
import { ActivatedRoute, Route, Router } from '@angular/router';
import { DialogDeleteComponent } from '../common/delete/dialogdelete.component';
import { Requerimiento } from '../model/requerimiento';
import { Usuario } from '../model/usuario';
import { RequerimientoService } from '../services/requerimiento/requerimiento.service';
import { UsuarioService } from '../services/usuario/usuario.service';

@Component({
  selector: 'app-detallesrequerimiento',
  templateUrl: './detallesrequerimiento.component.html',
  styleUrls: ['./detallesrequerimiento.component.scss'],
})
export class DetallesRequerimientoComponent implements OnInit {

  requerimientos: MatTableDataSource<Requerimiento>;

  empleado: Usuario;

  realizados: number = 0;
  pendientes: number = 0;
  asignados: number = 0;

  mostrarRequerimientos: boolean = false;

  public columnas: string[] =  ['id','descripcion','responsable','estado',
  'fechaRegistro','actions'];

  @ViewChild(MatSort) sort: MatSort;
  @ViewChild(MatPaginator) paginator: MatPaginator;

  constructor(
    private _route: ActivatedRoute,
    private requerimientoService: RequerimientoService,
    private router: Router,
    private usuarioService: UsuarioService,
    private dialog: MatDialog,
    private snackBar: MatSnackBar
  ) {}

  ngOnInit() {
    let id_empleado = this._route.snapshot.paramMap.get('id');
    let numString = id_empleado != null ? id_empleado : '0';
    if (numString != '0') {
      let id = Number(numString);
      this.getRequerimientoByEmpleado(id);
      if(window.localStorage.getItem("empleado")!=null){
        let emple = window.localStorage.getItem("empleado");
        this.empleado = (emple != null ? JSON.parse(emple) : null);
      }
    } else {
      this.router.navigate(['/home']);
    }
  }

  getRequerimientoByEmpleado(id_empleado: number) {
    this.realizados = this.asignados= this.pendientes = 0;
    this.requerimientoService.getRequerimientosByEmpleado(id_empleado)
      .subscribe((response) => {
        if (response.length != 0) {
          this.mostrarRequerimientos = true;
          this.asignados = response.length;
          response.forEach(element => {
            if(element.estado=="realizado"){ this.realizados+=1; }
            if(element.estado=="pendiente"){ this.pendientes+=1; }
          });
          this.requerimientos = new MatTableDataSource(response);
          this.requerimientos.sort = this.sort;
          this.requerimientos.paginator = this.paginator;
        }else{
          console.log("vacio");
        }
      });
  }

  openDelete(id_requerimiento:number){
    const dialogRef = this.dialog.open(DialogDeleteComponent,{});
    dialogRef.afterClosed().subscribe(result => {
      if(result){
        this.requerimientoService.deleteRequerimiento(id_requerimiento).subscribe(response =>{
          if(response.exito == 1){
            this.snackBar.open("REQUERIMIENTO ELIMINADO CON EXITO",'',{duration:2000});
            this.getRequerimientoByEmpleado(this.empleado.id);
          }
        })
      }
    });
  }
}
