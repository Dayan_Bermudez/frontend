import { Component, Inject, OnInit } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { MatSnackBar } from '@angular/material/snack-bar';
import { MatTableDataSource } from '@angular/material/table';
import { NuevoRequerimiento } from 'src/app/model/createModel/nuevorequerimiento';
import { Requerimiento } from 'src/app/model/requerimiento';
import { Usuario } from 'src/app/model/usuario';
import { TokenService } from 'src/app/security/services/token.service';
import { RequerimientoService } from 'src/app/services/requerimiento/requerimiento.service';

@Component({
  selector: 'app-dialogrequerimiento',
  templateUrl: './dialogrequerimiento.component.html',
  styleUrls: ['./dialogrequerimiento.component.scss']
})
export class DialogRequerimientoComponent implements OnInit {

  nuevoRequerimiento: NuevoRequerimiento;

  filtroEstado: number;
  filtroNombre: string;

  nombre: string = '';
  correo: string = '';

  empleados: MatTableDataSource<Usuario>;

  public columnas: string[] =  ['nombre','identificacion','correoElectronico','actions'];

  constructor(
    private dialogRef: MatDialogRef<DialogRequerimientoComponent>,
    @Inject(MAT_DIALOG_DATA) public requerimiento: Requerimiento,
    private requerimientoService: RequerimientoService,
    private snackBar: MatSnackBar,
    private tokenService: TokenService
  ) { }

  ngOnInit() {
    if(this.requerimiento!=null){
      this.nuevoRequerimiento = new NuevoRequerimiento();
      this.asignarEmpleado(this.requerimiento.empleado);
      this.nuevoRequerimiento.descripcion = this.requerimiento.descripcion;
      this.nuevoRequerimiento.empleado_id = this.requerimiento.empleado.id;
      this.nuevoRequerimiento.responsable_id = this.requerimiento.responsable.id;
    }else{
      this.nuevoRequerimiento = new NuevoRequerimiento();
      this.nuevoRequerimiento.descripcion = '';
      this.nuevoRequerimiento.empleado_id = 0;
      this.nuevoRequerimiento.responsable_id = 5;
    }
  }

  cerrarDialog(){
    this.dialogRef.close();
  }

  cambiarEstado(estado: number){
    this.filtroEstado = estado;
  }

  obtenerEmpleados(empleados: MatTableDataSource<Usuario>){
    this.empleados = empleados;
  }

  asignarEmpleado(empleado: any){
    this.nuevoRequerimiento.responsable_id = this.tokenService.getId(); 
    this.nuevoRequerimiento.empleado_id = empleado.id;
    this.nombre = empleado.nombre;
    if(empleado.segundoNombre!=null){
      this.nombre += ' '+empleado.segundoNombre;
    }
    this.nombre += ' '+empleado.apellido;
    if(empleado.segundoApellido!=null){
      this.nombre += ' '+empleado.segundoApellido;
    }
    this.correo = empleado.correoElectronico;
  }

  actualizar(){
    console.log(this.nuevoRequerimiento);
    this.requerimientoService.updateRequerimiento(this.requerimiento.id,this.nuevoRequerimiento)
    .subscribe(response =>{
      if(response.exito=1){
        this.snackBar.open("REQUERIMIENTO ACTUALIZADO",'',{duration:2000});
      }else{
        this.snackBar.open("NO SE PUDE ACTUALIZAR EL REQUERIMIENTO",'',{duration:2000});
      }
    });
    this.cerrarDialog();
  }

  registrar(){
    this.requerimientoService.createRequerimiento(this.nuevoRequerimiento)
    .subscribe(response =>{
      if(response.exito=1){
        this.snackBar.open("REQUERIMIENTO REGISTRADO",'',{duration:2000});
      }else{
        this.snackBar.open("NO SE PUDE REGISTRAR EL REQUERIMIENTO",'',{duration:2000});
      }
    });
    this.cerrarDialog();
  }

}
