import { Component, OnInit, ViewChild } from '@angular/core';
import { MatDialog } from '@angular/material/dialog';
import { MatPaginator } from '@angular/material/paginator';
import { MatSnackBar } from '@angular/material/snack-bar';
import { MatSort } from '@angular/material/sort';
import { MatTableDataSource } from '@angular/material/table';
import { Router } from '@angular/router';
import { DialogDeleteComponent } from '../common/delete/dialogdelete.component';
import { Requerimiento } from '../model/requerimiento';
import { RequerimientoDeUsuario } from '../model/requerimientodeusuario';
import { RequerimientoService } from '../services/requerimiento/requerimiento.service';
import { RequerimientoDeUsuarioService } from '../services/requerimiento/requerimientodeusuario.service';
import { DialogRequerimientoComponent } from './dialog/dialogrequerimiento/dialogrequerimiento.component';

@Component({
  selector: 'app-requerimientodeusuario',
  templateUrl: './requerimientodeusuario.component.html',
  styleUrls: ['./requerimientodeusuario.component.scss']
})
export class RequerimientoDeUsuarioComponent implements OnInit {

  mostrarRequerimientos: boolean = false;

  upRequerimiento: Requerimiento;
  requerimientos: MatTableDataSource<Requerimiento>;
  requerimientosDeUsuario: MatTableDataSource<RequerimientoDeUsuario>;

  @ViewChild(MatSort) sort: MatSort;
  @ViewChild(MatPaginator) paginator: MatPaginator;

  public columnas: string[] =  ['id','descripcion','responsable','estado',
  'fechaRegistro','actions'];

  constructor(
    private requerimientoDeUsuarioService: RequerimientoDeUsuarioService,
    private snackBar: MatSnackBar,
    private router: Router,
    private requerimientoService: RequerimientoService,
    private dialog: MatDialog
  ) { }

  ngOnInit() {
    this.getRequerimientos();
  }

  getRequerimientos(){
    this.requerimientoService.getRequerimientos().subscribe(response =>{
      if(response.length!=0){
        this.requerimientos = new MatTableDataSource(response);
        this.requerimientos.sort = this.sort;
        this.requerimientos.paginator = this.paginator;
        this.mostrarRequerimientos = true;
      }else{
        this.snackBar.open("NO SE HAN CARGADO LOS REQUERIMIENTOS",'',{duration:2000});
      }
    });
  }

  getRequerimientoDeUsuario(){
    this.requerimientoDeUsuarioService.getRequerimientosDeUsuario().subscribe(response =>{
      if(response!=null){
        this.requerimientosDeUsuario = new MatTableDataSource(response);
        this.requerimientosDeUsuario.sort = this.sort;
        this.requerimientosDeUsuario.paginator = this.paginator;
      }else{
        this.snackBar.open("NO SE HAN CARGADO LOS REQUERIMIENTOS DE LOS USUARIOS",'',{duration:2000});
      }
    })
  }

  openDelete(id_requerimiento:number){
    const dialogRef = this.dialog.open(DialogDeleteComponent,{});
    dialogRef.afterClosed().subscribe(result => {
      if(result){
        this.requerimientoService.deleteRequerimiento(id_requerimiento).subscribe(response =>{
          if(response.exito == 1){
            this.snackBar.open("REQUERIMIENTO ELIMINADO CON EXITO",'',{duration:2000});
            this.getRequerimientos();
          }
        })
      }
    });
  }

  openNewDialog(){
    const dialogRef = this.dialog.open(DialogRequerimientoComponent,{
      width: '90%',
      height:'90%'
    });
    dialogRef.afterClosed().subscribe(response => {
      this.getRequerimientos()
    });
  }

  openEditDialog(requerimiento: Requerimiento){
    const dialogRef = this.dialog.open(DialogRequerimientoComponent,{
      width: '90%',
      height:'90%',
      data:  requerimiento
    });
    dialogRef.afterClosed().subscribe(response => {
      this.getRequerimientos()
    });
  }
}
