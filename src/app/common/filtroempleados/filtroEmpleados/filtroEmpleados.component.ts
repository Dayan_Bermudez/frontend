import { Component, EventEmitter, OnInit, Output, ViewChild } from '@angular/core';
import { MatPaginator } from '@angular/material/paginator';
import { MatSnackBar } from '@angular/material/snack-bar';
import { MatSort } from '@angular/material/sort';
import { MatTableDataSource } from '@angular/material/table';
import { TipoIdentificacion } from 'src/app/model/tipoIdentificacion';
import { Usuario } from 'src/app/model/usuario';
import { TipoIdentificacionService } from 'src/app/services/tipoIdentificacion/tipoidentificacion.service';
import { UsuarioService } from 'src/app/services/usuario/usuario.service';

@Component({
  selector: 'app-filtroEmpleados',
  templateUrl: './filtroEmpleados.component.html',
  styleUrls: ['./filtroEmpleados.component.scss']
})
export class FiltroEmpleadosComponent implements OnInit {


  filtro: string = '1';
  filtroNombre: string;
  filtroApellido: string;
  filtroSegundoNombre: string;
  filtroSegundoApellido: string;
  filtroCorreo: string;
  filtroIdentificacion: string;
  filtroTipo: number = 1;
  filtroEstado: number = 1;

  @Output() estadoFiltro = new EventEmitter<number>();
  @Output() empleadosResponse = new EventEmitter<MatTableDataSource<Usuario>>();

  empleados: MatTableDataSource<Usuario>;

  tiposIdentificacion: TipoIdentificacion[] = [];

  @ViewChild(MatSort) sort: MatSort;
  @ViewChild(MatPaginator) paginator: MatPaginator;

  constructor(
    private usuarioService: UsuarioService,
    private snackBar: MatSnackBar,
    private tipoIdentificacionService: TipoIdentificacionService
  ) { }

  ngOnInit() {
    this.getTiposId();
  }

  getTiposId(){
    this.tipoIdentificacionService.getTipos().subscribe(response =>{
      if(response.length!=0){
        this.tiposIdentificacion = response;
      }else{
        this.snackBar.open("HUBO UN PROBLEMA",'',{duration:2000});
      }
    });
  }

  getEmpleados(){
    this.usuarioService.getUsuarios().subscribe(response => {
      if(response.length!=0){
        this.paginatorFuncion(response);
      }else{
        this.snackBar.open("NO SE HAN CARGADO LOS EMPLEADOS",'',{duration:2000});
      }
    });
  }

  paginatorFuncion(response: Usuario[]){
    this.empleados = new MatTableDataSource(response);
    this.empleados.sort = this.sort;
    this.empleados.paginator = this.paginator;
    this.result();
  }

  limpiar(){
    this.getEmpleados();
    this.filtro = '1';
    this.filtroNombre = '';
    this.filtroApellido = '';
    this.filtroSegundoNombre = '';
    this.filtroSegundoApellido = '';
    this.filtroCorreo = '';
    this.filtroIdentificacion = '';
    this.filtroTipo = 0;
    this.filtroEstado = 1;
  }

  filtrar(){
    switch (this.filtro) {
      case '1':
        if(this.filtroNombre!=null){
          if(this.filtroNombre.length!=0){
            this.usuarioService.getUsuarioByNombre(this.filtroNombre).subscribe(response=>{
              if(response.length!=0){
                this.paginatorFuncion(response);
              }else{
                this.usuariosNoOptenidosDelFiltro();
              }
            });
          }else{
            this.noSeAplicaronFiltros();
          }
        }else{
          this.noSeAplicaronFiltros();
        }
        break;
      
      case '2':
        if(this.filtroSegundoNombre!=null){
          if(this.filtroSegundoNombre.length!=0){
            this.usuarioService.getUsuarioBySegundoNombre(this.filtroSegundoNombre).subscribe(response=>{
              if(response.length!=0){
                this.paginatorFuncion(response);
              }else{
                this.usuariosNoOptenidosDelFiltro();
              }
            });
          }else{
            this.noSeAplicaronFiltros();
          }
        }else{
          this.noSeAplicaronFiltros();
        }
        break;
      
      case '3':
        if(this.filtroApellido!=null){
          if(this.filtroApellido.length!=0){
            this.usuarioService.getUsuarioByApellido(this.filtroApellido).subscribe(response=>{
              if(response.length!=0){
                this.paginatorFuncion(response);
              }else{
                this.usuariosNoOptenidosDelFiltro();
              }
            });
          }else{
            this.noSeAplicaronFiltros();
          }
        }else{
          this.noSeAplicaronFiltros();
        }
        break;
      
      case '4':
        if(this.filtroSegundoApellido!=null){
          if(this.filtroSegundoApellido.length!=0){
            this.usuarioService.getUsuarioBySegundoApellido(this.filtroSegundoApellido).subscribe(response=>{
              if(response.length!=0){
                this.paginatorFuncion(response);
              }else{
                this.usuariosNoOptenidosDelFiltro();
              }
            });
          }else{
            this.noSeAplicaronFiltros();
          }
        }else{
          this.noSeAplicaronFiltros();
        }
        break;

      case '5':
        this.usuarioService.getUsuarioByTipoId(Number(this.filtroTipo)).subscribe(response=>{
          if(response.length!=0){
            this.paginatorFuncion(response);
          }else{
            this.usuariosNoOptenidosDelFiltro();
          }
        });
        break;

      case '6':
        if(this.filtroIdentificacion!=null){
          if(this.filtroIdentificacion.length!=0){
            this.usuarioService.getUsuarioByIdentificacion(this.filtroIdentificacion).subscribe(response=>{
              if(response.length!=0){
                this.paginatorFuncion(response);
              }else{
                this.usuariosNoOptenidosDelFiltro();
              }
            });
          }else{
            this.noSeAplicaronFiltros();
          }
        }else{
          this.noSeAplicaronFiltros();
        }
        break;
      
      case '7':
        if(this.filtroCorreo!=null){
          if(this.filtroCorreo.length!=0){
            this.usuarioService.getUsuarioByCorreoElectronico(this.filtroCorreo).subscribe(response=>{
              if(response.length!=0){
                this.paginatorFuncion(response);
              }else{
                this.usuariosNoOptenidosDelFiltro();
              }
            });
          }else{
            this.noSeAplicaronFiltros();
          }
        }else{
          this.noSeAplicaronFiltros();
        }
        break;

      case '8':
            this.usuarioService.getUsuarioByEstado(Number(this.filtroEstado)).subscribe(response=>{
              if(response.length!=0){
                this.paginatorFuncion(response);
              }else{
                this.usuariosNoOptenidosDelFiltro();
              }
            });
        break;
    
      default:
        
        break;
    }
  }

  result(){
    this.estadoFiltro.emit(this.filtroEstado);
    this.empleadosResponse.emit(this.empleados);
  }

  usuariosNoOptenidosDelFiltro(){
    this.snackBar.open("NO SE HAN ENCONTRADO USUARIOS CON EL FILTRO APLICADO",'',{duration:2000});
  }

  noSeAplicaronFiltros(){
    this.snackBar.open("NO SE APLICARON FILTROS",'',{duration:2000});
  }

}
