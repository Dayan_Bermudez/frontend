import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { TokenService } from '../security/services/token.service';

@Component({
  selector: 'app-menu',
  templateUrl: './menu.component.html',
  styleUrls: ['./menu.component.scss']
})
export class MenuComponent implements OnInit {

  isLoggedMenu = false;
  nombreUsuario: string = '';
  authoritie: string = '';
  admin: boolean = false;
  idEmpleado: number;

  constructor(
    private tokenService: TokenService,
    private router: Router
  ) { }

  ngOnInit() {
    if(this.tokenService.getToken()!='undefined'){
      this.isLoggedMenu = true;
      this.idEmpleado = this.tokenService.getId();
      this.nombreUsuario = this.tokenService.getUsername();
      this.authoritie = this.tokenService.getAuthorities()[0];
      let rol = this.tokenService.getAuthorities();
      if(rol[0]=='admin'){
        this.admin = true;
      }
    }
  }

  logOut(){
    this.isLoggedMenu = false;
    this.nombreUsuario = '';
    this.tokenService.logOut();
    this.router.navigate(['/login']);
  }

}
